import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Board {

    public int width;
    public int height;
    private final SlotType[][] slots;
    private final ArrayList<Point> availableSlots;
    private final Random random;

    public Board(int boardWidth, int boardHeight){
        width = boardWidth;
        height = boardHeight;
        slots = new SlotType[width][height];
        availableSlots = new ArrayList<>();
        random = new Random();
        reset();
    }

    public void reset(){
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++){
                slots[i][j] = SlotType.EMPTY;
                availableSlots.add(new Point(i, j));
            }
        }
    }

    public SlotType getSlot(int row, int column){
        return slots[row][column];
    }

    public void placePiece(PlayerColor color, int row, int column){
        switch (color){
            case RED -> {
                slots[row][column] = SlotType.RED;
                System.out.println("Placed Red Piece At (" + row + "," + column + ")");
            }
            case BLUE -> {
                slots[row][column] = SlotType.BLUE;
                System.out.println("Placed Blue Piece At (" + row + "," + column + ")");
            }
        }

        availableSlots.remove(new Point(row, column));
    }

    public void placeRandomPiece(PlayerColor color){
        Point random_slot = availableSlots.get(Math.abs(random.nextInt() % availableSlots.size()));

        switch (color){
            case RED -> {
                slots[random_slot.x][random_slot.y] = SlotType.RED;
                System.out.println("Placed Red Piece At (" + random_slot.x + "," + random_slot.y + ")");
            }
            case BLUE -> {
                slots[random_slot.x][random_slot.y] = SlotType.BLUE;
                System.out.println("Placed Blue Piece At (" + random_slot.x + "," + random_slot.y + ")");
            }
        }

        availableSlots.remove(random_slot);
    }

    public boolean isFull(){
        boolean isFull = true;
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                if (slots[i][j] == SlotType.EMPTY) {
                    isFull = false;
                    break;
                }
            }
        }
        return isFull;
    }

    public boolean isWinningBoard(){
        for(int i = 0; i < width; i++){
            SlotType slotType = slots[i][0];

            if(slotType == SlotType.EMPTY){
                continue;
            }

            boolean matchesColor = true;
            for(int j = 0; j < height; j++){
                if (slots[i][j] != slotType) {
                    matchesColor = false;
                    break;
                }
            }

            if(matchesColor){
                return true;
            }
        }

        return false;
    }
}
