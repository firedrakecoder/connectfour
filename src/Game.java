import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class Game {
    boolean running;
    Board board;
    GameWindow window;
    PlayerColor playerTurn;

    Game(){
        board = new Board(6,7);

        window = new GameWindow(board);
        window.setVisible(true);
        SwingUtilities.invokeLater(() -> {
            var frame = new JFrame("Connect Four");
            frame.setSize(1920, 1080);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.getContentPane().add(window, BorderLayout.CENTER);
            frame.setVisible(true);
        });

        playerTurn = PlayerColor.RED;
    }

    void quit(){
        running = false;
    }

    void run(){
        running = true;

        long previous = System.currentTimeMillis();
        double accumulator = 0;
        double TIME_STEP = 1000.0/5.0; //5 FPS

        while (running){
            long current = System.currentTimeMillis();
            long elapsed = current - previous;
            previous = current;

            accumulator += elapsed;

            while (accumulator >= TIME_STEP){
                update();
                accumulator -= TIME_STEP;
                window.repaint();
            }
        }
    }

    void update(){
        switch (playerTurn){
            case RED -> {
                board.placeRandomPiece(PlayerColor.RED);
            }
            case BLUE -> {
                board.placeRandomPiece(PlayerColor.BLUE);
            }
        }

        if(board.isWinningBoard()){
            //board.reset();
            System.out.println("Winner!");
            //quit();
        }

        if(board.isFull()){
            quit();
        }

        switchPlayer();
    }

    void switchPlayer(){
        switch (playerTurn){
            case RED -> {
                playerTurn = PlayerColor.BLUE;
            }
            case BLUE -> {
                playerTurn = PlayerColor.RED;
            }
        }
    }
}
