import javax.swing.*;
import java.awt.*;

public class GameWindow extends JPanel {

    Board board;

    GameWindow(Board gameBoard){
        board = gameBoard;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        //TODO Replace With Launcher or Config
        int screenWidth = 1920;
        int screenHeight = 1080;

        int cellWidth = screenWidth/board.width;
        int cellHeight = screenHeight/board.height;

        for(int i = 0; i < board.width; i++){
            for(int j = 0; j<board.height; j++){
                SlotType slotType = board.getSlot(i, j);
                switch (slotType){
                    case RED -> {
                        g.setColor(Color.RED);
                    }
                    case BLUE -> {
                        g.setColor(Color.BLUE);
                    }
                    case EMPTY -> {
                        g.setColor(Color.WHITE);
                    }
                }
                g.fillRect(i * cellWidth,j * cellHeight,cellWidth,cellHeight);
                g.setColor(Color.BLACK);
                g.drawRect(i * cellWidth,j * cellHeight,cellWidth,cellHeight);
            }
        }
    }


}
